'use strict';

// use winston for logging
const winston = require('winston');

// set up file transport and export logger
module.exports = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({ filename: `${__dirname}/../logs/log.txt` })
    ]
});