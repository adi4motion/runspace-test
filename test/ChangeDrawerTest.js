'use strict';

const assert = require('assert');
const ChangeDrawer = require('../models/ChangeDrawer');

describe('ChangeDrawer', function () {

    describe('#availableDenominations', function () {

        it('should return available denominations', function () {
            assert.deepEqual(ChangeDrawer.availableDenominations, {
                FIFTY_PENCE: 50,
                FIVE_PENCE: 5,
                ONE_PENNY: 1,
                ONE_POUND: 100,
                TEN_PENCE: 10,
                TWENTY_PENCE: 20,
                TWO_PENCE: 2
            });
        });
    });

    describe('#getCoins', function () {

        it('should be able to retrieve any change amount in unlimited coins mode', function (done) {
            const changeDrawer = ChangeDrawer.getChangeDrawer(true);
            changeDrawer.getCoins(100, 1000000, function (error, coins) {
                assert.equal(error, null);
                assert.deepEqual(coins, {
                    coin: {
                        denomination: 100
                    },
                    quantity: 1000000
                });
                done();
            });
        });

        it('should error for invalid denomination', function (done) {
            const changeDrawer = ChangeDrawer.getChangeDrawer(true);
            changeDrawer.getCoins(488, 1000000, function (error) {
                assert.equal(error.message, 'Requested denomination does not exist in the drawer');
                done();
            });
        });

        it('should return maximum quantity from inventory rather than requested amount', function (done) {
            const changeDrawer = ChangeDrawer.getChangeDrawer(false);
            changeDrawer.getCoins(100, 1000000, function (error, coins) {
                assert.deepEqual(coins, {
                    coin: {
                        denomination: 100
                    },
                    quantity: 11
                });
                changeDrawer.putCoins(coins.coin.denomination, coins.quantity, function () {
                    done();
                });
            });
        });
    });
});