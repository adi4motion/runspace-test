'use strict';

const assert = require('assert');
const ChangeDispenser = require('../models/ChangeDispenser');

describe('ChangeDispenser', function () {

    describe('#getChangeFor', function () {

        it('should return minimum number of coins', function (done) {
            const changeDispenser = new ChangeDispenser(true);
            changeDispenser.getChangeFor(488, function (error, coins) {
                assert.deepEqual(coins, [
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 50
                    },
                    {
                        denomination: 20
                    },
                    {
                        denomination: 10
                    },
                    {
                        denomination: 5
                    },
                    {
                        denomination: 2
                    },
                    {
                        denomination: 1
                    }
                ]);
                done();
            });
        });

        it('should be able to dispense any amount in unlimited coins mode', function (done) {
            const changeDispenser = new ChangeDispenser(true);
            changeDispenser.getChangeFor(5000, function (error, coins) {
                assert.deepEqual(coins, [
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    },
                    {
                        denomination: 100
                    }
                ]);
                done();
            });
        });

        it('should NOT be able to dispense any amount in LIMITED coins mode', function (done) {
            const changeDispenser = new ChangeDispenser(false);
            changeDispenser.getChangeFor(5000, function (error) {
                assert.equal(error.message, 'Not enough pence in the inventory');
                done();
            });
        });
    });
});