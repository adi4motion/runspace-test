# About

This project implements the logic for a vending machine that will return the optimal change for a given number of pence. It exposes both CLI and API functionality.
The code is written entirely in JavaScript (ES6) and runs on NodeJS (8.x) platform. 

The API is using restify for request handling although this is not necessary for an API that exposes just 1 request, but it does help with writing less/cleaner code
as well as with request query and body parsing.

Only minimal unit testing (see test directory) and logging (see models/ChangeDispenser.js) are added.

Error is thrown for insufficient coinage only in CLI mode, see cli.js line 41 for details.

The REST API allows only coin dispensing (not also updating the inventory; I assumed this is not a requirement); 

# Structure

 -logs -- contains log files

 -models -- contains models
 
 --ChangeDispenser.js -- model that dispenses change
 
 --ChangeDrawer.js -- model that handles the inventory
 
 --Coin.js -- coin model
 
 -resources
 
 --coin-inventory.properties -- inventory source
 
 -test -- contains unit tests
 
 --ChangeDispenserTest.js -- unit tests for ChangeDispenser.js model
 
 --ChangeDrawerTest.js -- unit tests for ChangeDrawer.js model
 
 -utils
 
 --logger.js -- Log utility
 
 -api.js -- API server
 
 -cli.js -- Command Line Interface
 
 -package.json
 
 -package-lock.json
 
 -readme.md

# Install

  - npm install
  
# Run CLI

  - node cli.js -pence amountOfPence [unlimited]
  
  E.g. node cli.js -pence 156
  
  or: node cli.js -pence 156 unlimited (for unlimited coins mode)
  
# Run Server

- npm start

The server will listen on port 8080 and will expose a POST request on /api/dispense path.
POST body accepts a pence integer { "pence": 156 }. The request allows for an optional 'mode' query parameter that switches to unlimited coin mode on set to 'unlimited' (e.g. ?mode=unlimited)

E.g.

POST 127.0.0.1:8080/api/dispense?mode=unlimited
{
    "pence": 345
}

CURL request example

curl -X POST \
  'http://192.168.0.188:8080/api/dispense?mode=unlimited' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "pence": 345
}'