'use strict';

// load dependencies
const fs = require('fs');
const Coin = require('./Coin');

// define global vars
const UNLIMITED_QUANTITY = -1;
const availableDenominations = {
    ONE_POUND: 100,
    FIFTY_PENCE: 50,
    TWENTY_PENCE: 20,
    TEN_PENCE: 10,
    FIVE_PENCE: 5,
    TWO_PENCE: 2,
    ONE_PENNY: 1
};

// keep an unlimited and a limited change drawer
let unlimitedChangeDrawer;
let limitedChangeDrawer;

/**
 * Inventory item
 * @param coin
 * @param quantity
 * @constructor
 */
function InventoryItem(coin, quantity) {
    this.coin = coin;
    this.quantity = quantity;
}

/**
 * Read coin inventory file and build a coin collection
 * @param callback
 */
function getCoinsFromInventory(callback) {

    const coinCollection = {};

    // read coin inventory
    fs.readFile(`${__dirname}/../resources/coin-inventory.properties`, {encoding: 'utf-8'}, function (error, coinInventory) {
        // if error occurred, stop
        if (error) {
            return callback(error);
        }

        // build default coin collection, with 0 quantity
        const _availableDenominations = Object.values(availableDenominations);
        _availableDenominations.forEach(function (denomination) {
            coinCollection[denomination] = new InventoryItem(new Coin(denomination), 0);
        });

        // parse coin inventory
        const denominationCountRegExp = /(\d+)=(\d+)/g;
        let matches = denominationCountRegExp.exec(coinInventory);

        while (matches) {
            const denomination = parseInt(matches[1]);
            const quantity = parseInt(matches[2]);
            // update collection only if the denomination is valid
            if (_availableDenominations.includes(denomination)) {
                coinCollection[denomination].quantity = quantity;
            }
            matches = denominationCountRegExp.exec(coinInventory);
        }
        callback(null, coinCollection);
    });
}

/**
 * Update coin inventory (file)
 * @param coinInventory
 * @param callback
 */
function updateCoinInventory(coinInventory, callback) {
    let inventory = '';

    // use availableDenominations as a reference list (coinInventory should contain the same denominations, but play it safe)
    Object.keys(availableDenominations).forEach(function (denomination) {
        // separate denominations on rows
        if (inventory.length) {
            inventory += '\n';
        }

        let _denomination = availableDenominations[denomination];
        // if a denomination is present in the inventory, store it's quantity
        if (coinInventory[_denomination]) {
            inventory += `${_denomination}=${coinInventory[_denomination].quantity}`;
            // otherwise store it with 0 quantity
        } else {
            inventory += `${_denomination}=0`;
        }
    });
    fs.writeFile(`${__dirname}/../resources/coin-inventory.properties`, inventory, callback);
}

/**
 * Change Drawer
 * @param unlimitedCoins
 * @constructor
 */
function ChangeDrawer(unlimitedCoins) {

    // by default, assume limited number of coins
    unlimitedCoins = unlimitedCoins || false;

    let coins = {};

    /**
     * Update coin collection
     * @param callback
     */
    function updateCoinCollection(callback) {
        // if the drawer uses an unlimited number of coins and the coin collection was not initialized
        if (unlimitedCoins && !Object.keys(coins).length) {
            // initialize coin collection
            Object.keys(availableDenominations).forEach(function (denomination) {
                let _denomination = availableDenominations[denomination];
                coins[_denomination] = new InventoryItem(new Coin(_denomination), UNLIMITED_QUANTITY);
            });
            callback(null, coins);
            // drawer uses a limited number of coins
        } else if (!unlimitedCoins) {
            // read them from the inventory
            getCoinsFromInventory(function (error, coinCollection) {
                if (error) {
                    return callback(error);
                }
                coins = coinCollection;
                callback(null, coins);
            });
            // just continue
        } else {
            callback(null, coins);
        }
    }

    /**
     * Get coins from drawer
     * @param denomination
     * @param maxQuantity
     * @param callback
     */
    this.getCoins = function (denomination, maxQuantity, callback) {
        // ensure the coin collection is up to date
        updateCoinCollection(function (error) {
            if (error) {
                return callback(error);
            }
            const inventoryItem = coins[denomination];
            // check if the item is not valid
            if (!inventoryItem) {
                // stop with error
                callback(new Error('Requested denomination does not exist in the drawer'));
                // if the item is available in unlimited quantities
            } else if (inventoryItem.quantity === UNLIMITED_QUANTITY) {
                // send it
                callback(null, new InventoryItem(inventoryItem.coin, maxQuantity));
            } else {
                // if the available quantity is less the requested quantity
                if (inventoryItem.quantity < maxQuantity) {
                    // update requested quantity
                    maxQuantity = inventoryItem.quantity;
                }
                // update inventory quantity
                inventoryItem.quantity -= maxQuantity;
                // update inventory
                updateCoinInventory(coins, function (error) {
                    if (error) {
                        return callback(error);
                    }
                    callback(null, new InventoryItem(inventoryItem.coin, maxQuantity));
                });
            }
        });
    };

    /**
     * Put coins back into the inventory
     * @param denomination
     * @param quantity
     * @param callback
     */
    this.putCoins = function (denomination, quantity, callback) {
        // only update the inventory if it uses limited quantities
        if (!unlimitedCoins) {
            // ensure the coin collection is up to date
            updateCoinCollection(function (error) {
                if (error) {
                    return callback(error);
                }
                const inventoryItem = coins[denomination];
                if (inventoryItem) {
                    inventoryItem.quantity += quantity;
                }
                updateCoinInventory(coins, callback);
            });
        } else {
            callback(null);
        }
    };
}

module.exports = {
    /**
     * Get instance of Change Drawer
     * @param unlimitedCoins
     * @returns {*}
     */
    getChangeDrawer: function (unlimitedCoins) {
        let changeDrawer;
        if (unlimitedCoins) {
            if (!unlimitedChangeDrawer) {
                unlimitedChangeDrawer = new ChangeDrawer(unlimitedCoins);
            }
            changeDrawer = unlimitedChangeDrawer;
        } else {
            if (!limitedChangeDrawer) {
                limitedChangeDrawer = new ChangeDrawer(unlimitedCoins);
            }
            changeDrawer = limitedChangeDrawer;
        }
        return changeDrawer;
    },
    availableDenominations: availableDenominations
};