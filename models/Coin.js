'use strict';

/**
 * Coin
 * @param denomination
 * @constructor
 */
function Coin (denomination) {
    this.denomination = denomination;
}

module.exports = Coin;