'use strict';

// load dependencies
const ChangeDrawer = require('./ChangeDrawer');
const log = require('../utils/logger');

/**
 * Change Dispenser
 * @param unlimitedCoins
 * @constructor
 */
function ChangeDispenser(unlimitedCoins) {
    // get change drawer
    const changeDrawer = ChangeDrawer.getChangeDrawer(unlimitedCoins);
    // get a list of sorted denominations, from highest to lowers
    const sortedDenominations = Object.values(ChangeDrawer.availableDenominations).sort(function (a, b) {
        a = parseInt(a);
        b = parseInt(b);
        let result = 0;
        if (a => b) {
            result = -1;
        } else {
            result = 1;
        }
        return result;
    });

    /**
     * Dispense pence
     * @param pence The amount of pence to be dispensed
     * @param denominations The list of available denominations
     * @param coins List of dispensed coins
     * @param callback
     */
    function dispense(pence, denominations, coins, callback) {
        // if the amount was not dispensed and there are still denominations left
        if (pence > 0 && denominations.length) {
            // get the highest denomination available
            const denomination = denominations.shift();
            // get needed number of current denomination
            let denominationCount = parseInt(pence / denomination);
            // if current denomination is usable
            if (denominationCount > 0) {
                // extract the denomination from the drawer
                changeDrawer.getCoins(denomination, denominationCount, function (error, coinsFromDrawer) {
                    if (error) {
                        return callback(error, pence, coins);
                    }
                    // the actual denomination count is the one received from the drawer (drawer may not have the full amount)
                    denominationCount = coinsFromDrawer.quantity;
                    // if something was retrieved from the drawer
                    if (denominationCount) {
                        // update coin collection
                        coins.push(coinsFromDrawer);
                        // update amount of pence left to be dispensed
                        pence = (pence - denomination * denominationCount);
                    }
                    // continue dispensing
                    dispense(pence, denominations, coins, callback);
                });
                // continue dispensing
            } else {
                dispense(pence, denominations, coins, callback);
            }
            // amount dispensed or no more denominations left
        } else {
            callback(null, pence, coins);
        }
    }

    /**
     * Rollback (dispense) transaction
     * @param coins
     * @param callback
     */
    function rollbackTransaction(coins, callback) {
        // if there are coins to be added back
        if (coins.length) {
            // get first item from collection and add it back
            const item = coins.shift();
            changeDrawer.putCoins(item.coin.denomination, item.quantity, function (error) {
                if (error) {
                    return callback(error);
                }
                // continue rollback
                rollbackTransaction(coins, callback);
            });
        // rollback finished
        } else {
            callback(null);
        }
    }

    /**
     * Get change for a specified amount of pence
     * @param pence
     * @param callback
     */
    this.getChangeFor = function (pence, callback) {
        // use a transactioId for logging (async actions may result in out-of-order log messages)
        const transactionId = Buffer.from(Math.random().toString()).toString('base64');
        // clone denominations (destructive operations will be performed)
        const _denominations = sortedDenominations.slice();
        // log usage
        log.info(`[${transactionId}] Trying to dispense ${pence} pence`);
        // dispense denomination
        dispense(pence, _denominations, [], function (error, pence, coins) {
            // if error occurred while dispensing
            if (error) {
                log.error(`[${transactionId}] Error while dispensing ${pence} pence`, error);
                // if some coins were already dispensed
                if (coins) {
                    log.info(`[${transactionId}] Rolling back transaction. Sending back dispensed coins`);
                    // rollback
                    rollbackTransaction(coins, function (rollbackError) {
                        log.error(`[${transactionId}] Error while rolling back transaction`, rollbackError);
                        if (rollbackError) {
                            callback(rollbackError);
                        } else {
                            callback(error);
                        }
                    });
                } else {
                    // just stop with error
                    callback(error);
                }
            } else {
                if (pence === 0) {
                    log.info(`[${transactionId}] Successfully dispensed the amount`);
                    // the result should be a collection of coins
                    const result = [];
                    // go through the inventory
                    coins.forEach(function (inventoryItem) {
                        // as long as there are coins left
                        while (inventoryItem.quantity) {
                            // push them into the result
                            result.push(inventoryItem.coin);
                            inventoryItem.quantity--;
                        }
                    });
                    // dispense successful
                    callback(null, result);
                } else {
                    log.error(`[${transactionId}] Not enough pence in the inventory to dispense the requested amount. Rolling back...`, error);
                    // rollback
                    rollbackTransaction(coins, function (rollbackError) {
                        log.error(`[${transactionId}] Error while rolling back transaction`, rollbackError);
                       if (rollbackError) {
                           callback(rollbackError);
                       } else {
                           callback(new Error('Not enough pence in the inventory'));
                       }
                    });
                }
            }
        });
    }
}

module.exports = ChangeDispenser;