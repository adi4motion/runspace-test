'use strict';

// load dependencies
const restify = require('restify');
const ChangeDispenser = require('./models/ChangeDispenser');

// use restify for request handling (node's built in HTTP module would've suffice for an API with one route, but I've used restify's body/query parsing as well)
const server = restify.createServer();
server.use(restify.plugins.queryParser({mapParams: false}));
server.use(restify.plugins.bodyParser());

// enhance response with some methods
server.use(function (req, res, next) {
    res.error = function (code, error) {
        res.send(code, {error: error.message});
    };
    res.ok = function (body) {
        res.send(200, body);
    };
    next();
});

// add /api/dispense route
server.post('/api/dispense', function (req, res) {
    // get run mode
    const unlimitedCoins = (req.query.mode === 'unlimited');
    // do some basic request validation
    if (!req.body.pence || typeof req.body.pence !== 'number' || !Number.isInteger(req.body.pence)) {
        // bad request
        res.error(400, new Error('\'pence\' parameter is a required integer'));
    } else {
        // get pence dispenser
        const changeDispenser = new ChangeDispenser(unlimitedCoins);
        // get change for specified amounts of pence
        changeDispenser.getChangeFor(req.body.pence, function (error, coins) {
            // if error occurred, send error back to client
            if (error) {
                res.error(500, error);
            } else {
                // success
                res.ok({
                    mode: unlimitedCoins ? 'UNLIMITED' : 'LIMITED',
                    coins: coins
                });
            }
        })
    }
});

server.listen(8080, function () {
    console.log('%s listening at %s', server.name, server.url);
});