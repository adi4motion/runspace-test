'use strict';

// load dependencies
const ChangeDispenser = require('./models/ChangeDispenser');

const command = process.argv.join(' ');
const penceAmountRegExp = /-pence\s*(\d+)/g;
const matches = penceAmountRegExp.exec(command);

/**
 * Output message
 * @param error {boolean} Is the message an error
 * @param message {string} Message to be written
 * @param stop {boolean} Stop process after writing message
 */
function output(error, message, stop) {
    stop = stop || false;
    process[error ? 'stderr' : 'stdout'].write(`${message}\n`);
    if (stop) {
        process.exit(error ? 1 : 0);
    }
}

// check usage
if (!matches) {
    output(true, 'No amount of pence spcified.\n\nUsage: node cli.js -pence numberOfPence [unlimited]\nE.g. node cli.js -pence 156 unlimited\n', true);
}

// get pence amount
const pence = matches[1];
// get mode: limited vs unlimited coins
const unlimitedCoins = (command.indexOf('unlimited') !== -1);

output(false, `Dispensing ${pence} pence in ${unlimitedCoins ? 'UN' : ''}LIMITED coins mode`);

const changeDispenser = new ChangeDispenser(unlimitedCoins);

// dispense coins
changeDispenser.getChangeFor(pence, function (error, coins) {
    if (error) {
        /** I've decided to throw the error (in Javascript exceptions are called errors) from here only; it would have
         * been thrown in ChangeDispenser.getChangeFor() method but due to having async code, it would've been messy to
         * handle the error thrown there (it would've been caught using process.on('error') or restify.on('error') or
         * using a domain)
         * Instead, I've decided to handle the error via callbacks for usual flow and only throw the error for CLI mode to exemplify how the error is thrown)
         */
        if (error.message && error.message.startsWith('Not enough pence in the inventory')) {
            throw error;
        } else {
            output(true, error.message, true);
        }
    } else {
        output(false, JSON.stringify(coins, null, 2), true);
    }
});